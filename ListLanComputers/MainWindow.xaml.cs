﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace ListLanComputers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private Task Listing { get; set; }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Listing = listLanPCs();
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Listing.IsCompleted)
            {
                Listing.Dispose();
            }
        }


        private async Task listLanPCs()
        {
            #region 1 способ (получает совсем все, много лишнего)
            //await Task.Run(() =>
            //{
            //    DirectoryEntry root = new DirectoryEntry("WinNT:");
            //    foreach (DirectoryEntry computers in root.Children)
            //    {
            //        foreach (DirectoryEntry computer in computers.Children)
            //        {
            //            if (computer.Name != "Schema")
            //            {
            //                Application.Current.Dispatcher.Invoke(() => {
            //                    ListBox_LanComputers.Items.Add(computer.Name);
            //                });
            //            }
            //        }
            //    }
            //});
            #endregion

            #region 2 способ (получает только подключенные компы, бывают проблемы с именами)
            await Task.Factory.StartNew(() =>
            {
                Process proc = new Process();   // This creates a new process
                proc.StartInfo.FileName = "cmd"; // Tells it to launch a command prompt

                //  This line runs a command called "net view"
                //  which is a built in windows command that returns all the shares
                //  on a network
                proc.StartInfo.Arguments = "/C net view";

                // This property redirects the output of the command ran
                // to the StandardOutput object we use later
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.UseShellExecute = false;

                // This tells the program to show the command window or not
                // set to true to hide the window
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();

                // Sets all the output into a string
                string data = proc.StandardOutput.ReadToEnd();
                int start = 0;
                int stop = 0;

                // This parses through the output string
                // and grabs each share and outputs it.
                // you can save the strings into an array and add 
                // them to a list box or something if you wanted to.
                while (true)
                {
                    start = data.IndexOf('\\', start);
                    if (start == -1)
                        break;
                    start += 2;
                    stop = data.IndexOf('\n', start);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        ListBox_LanComputers.Items.Add(data.Substring(start, stop - start));
                    });
                    start = stop;
                }
            });
            #endregion

        }
    }
}
